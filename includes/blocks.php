<?php
namespace teik\Theme\Blocks;

return [
  HomeSlider::instance(),
  Choose::instance(),
  Offer::instance(),
  AboutCompany::instance(),
  Partners::instance(),
  Traders::instance(),
  Video::instance(),
  Promo::instance(),
  Opinions::instance(),
  Realizations::instance(),
  Paragraph::instance(),
  Qualities::instance(),
  OfferHorizontal::instance(),
  OurTeam::instance(),
  Montaz::instance(),
  Gallery::instance(),
  Contact::instance(),
  ContactForm::instance(),
  BigImage::instance(),
  ListLinks::instance(),
  StackImages::instance(),
  TextOnImage::instance(),
  LineDescription::instance(),
  ContentCover::instance(),
  ContentButtonImg::instance(),
  ImageContent::instance(),
  Design::instance(),
  TwoImages::instance(),
  SimpleList::instance(),
  Specs::instance(),
  BestOpinions::instance(),
  TextColumns::instance(),
  ImageAndCover::instance(),
  ContentAndTiles::instance(),
  Folders::instance(),
];