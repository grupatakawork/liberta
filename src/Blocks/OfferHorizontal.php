<?php
namespace teik\Theme\Blocks;

use teik\Theme\Traits\Singleton;

class OfferHorizontal extends AbstractBlock
{
  use Singleton;

  public $name = 'offerHorizontal';
  public $title = 'Oferta (poziomo)';
}