<?php
namespace teik\Theme\Blocks;

use teik\Theme\Traits\Singleton;

class Promo extends AbstractBlock
{
  use Singleton;

  public $name = 'promo';
  public $title = 'Promocje';
}