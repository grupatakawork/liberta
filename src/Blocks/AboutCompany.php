<?php
namespace teik\Theme\Blocks;

use teik\Theme\Traits\Singleton;

class AboutCompany extends AbstractBlock
{
  use Singleton;

  public $name = 'aboutCompany';
  public $title = 'O firmie';
}