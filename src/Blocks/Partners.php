<?php
namespace teik\Theme\Blocks;

use teik\Theme\Traits\Singleton;

class Partners extends AbstractBlock
{
  use Singleton;

  public $name = 'partners';
  public $title = 'Partnerzy';
}