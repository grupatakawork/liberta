<?php
namespace teik\Theme\Blocks;

use teik\Theme\Traits\Singleton;

class Offer extends AbstractBlock
{
  use Singleton;

  public $name = 'offer';
  public $title = 'Oferta';
}