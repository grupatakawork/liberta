<?php
namespace teik\Theme\Blocks;

use teik\Theme\Traits\Singleton;

class Choose extends AbstractBlock
{
  use Singleton;

  public $name = 'choose';
  public $title = 'Wybierz okna i drzwi';
}