<?php
namespace teik\Theme\Blocks;

use teik\Theme\Traits\Singleton;

class Video extends AbstractBlock
{
  use Singleton;

  public $name = 'video';
  public $title = 'Video';
}