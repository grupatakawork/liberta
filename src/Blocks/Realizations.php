<?php
namespace teik\Theme\Blocks;

use teik\Theme\Traits\Singleton;

class Realizations extends AbstractBlock
{
  use Singleton;

  public $name = 'realizations';
  public $title = 'Realizacje';
}