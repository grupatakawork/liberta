<?php
namespace teik\Theme\Blocks;

use teik\Theme\Traits\Singleton;

class Traders extends AbstractBlock
{
  use Singleton;

  public $name = 'traders';
  public $title = 'Zamówienie(Kontakt)';
}