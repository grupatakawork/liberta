<?php
namespace teik\Theme\Blocks;

use teik\Theme\Traits\Singleton;

class Opinions extends AbstractBlock
{
  use Singleton;

  public $name = 'opinions';
  public $title = 'Opinie klientów';
}