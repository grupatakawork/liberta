<?php
namespace teik\Theme\Blocks;

use teik\Theme\Traits\Singleton;

class Qualities extends AbstractBlock
{
  use Singleton;

  public $name = 'qualities';
  public $title = 'Cechy';
}