<?php
namespace teik\Theme\Blocks;

use teik\Theme\Traits\Singleton;

class Paragraph extends AbstractBlock
{
  use Singleton;

  public $name = 'paragraph';
  public $title = 'Paragraf';
}