<?php 
namespace teik\Theme\Blocks; 
 
use teik\Theme\Traits\Singleton; 
 
class Gallery extends AbstractBlock 
{ 
  use Singleton; 
 
public $name = 'gallery'; 
public $title = 'Gallery'; 
} 
