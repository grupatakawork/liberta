<?php
namespace teik\Theme\Blocks;

use teik\Theme\Traits\Singleton;

class HomeSlider extends AbstractBlock
{
  use Singleton;

  public $name = 'homeSlider';
  public $title = 'Home Slider';
}