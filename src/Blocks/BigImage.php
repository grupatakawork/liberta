<?php 
namespace teik\Theme\Blocks; 
 
use teik\Theme\Traits\Singleton; 
 
class BigImage extends AbstractBlock 
{ 
  use Singleton; 
 
public $name = 'bigImage'; 
public $title = 'BigImage'; 
} 
