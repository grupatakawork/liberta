<?php 
namespace teik\Theme\Blocks; 
 
use teik\Theme\Traits\Singleton; 
 
class SimpleList extends AbstractBlock 
{ 
  use Singleton; 
 
public $name = 'simpleList'; 
public $title = 'Simple List'; 
} 
