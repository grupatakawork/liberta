window.onload = ()=>{
    // let toggleopinions = document.getElementById("toggleopinions");
    
    // toggleopinions.addEventListener("click", (e)=> {
        //     e.preventDefault();
        //     toggleForm();
        // });
        
        // opinions.addEventListener("click", (e)=> {
            //     e.preventDefault();
            //     if(e.target.classList.contains("opinions__form")) {
                //         toggleForm();
                //     }
                // });
                
                
    let opinions = document.querySelector(".opinions__form");

    let buttonShowForm = document.querySelectorAll(".buttonShowForm");

    buttonShowForm.forEach(button => {
        button.addEventListener("click", (click)=> {
            if(click.target.classList.contains("buttonShowForm")) {
                click.preventDefault();
                console.log(click.target);
                showForm();
            }
        })
    });

    let buttonHideForm = document.querySelectorAll(".buttonHideForm");

    buttonHideForm.forEach(button => {
        button.addEventListener("click", (click)=> {
            if(click.target.classList.contains("buttonHideForm")) {
                click.preventDefault();
                console.log(click.target);
                hideForm();
            }
        })
    });

    function showForm() {
        opinions.classList.add("visible");
    }
    function hideForm() {
        opinions.classList.remove("visible");
    }


    // gallery ===================================================================

    let expandGalleryButtons = document.querySelectorAll(".gallery__expand");

    let galleries = [];
    expandGalleryButtons.forEach(button => {
        galleries.push({button: button, hiddenImages: button.parentNode.querySelectorAll(".gallery--hiddenImage")});
    })

    galleries.forEach(gal => {
        gal.button.addEventListener("click", ()=> {
            gal.button.classList.toggle("gallery--expanded")
            gal.hiddenImages.forEach(image => {
                image.classList.toggle("gallery--hiddenImage");
            })
        })
    })
}


// promocje - bigImage--interactive =============================================

let interactives = document.querySelectorAll(".bigImage--interactive");

interactives.forEach(promo => {
    let cover = promo.querySelector(".bigImage__cover");
    cover.addEventListener("click", click => {
        click.preventDefault();
        promo.classList.toggle("bigImage--open");
    })
})

document.querySelectorAll(".imageContent--expandable").forEach(container => {

    let button = container.querySelector(".linkbutton--expandable");
    button.addEventListener("click", (e)=>{
        e.preventDefault();
        container.classList.toggle("imageContent--collapsed")
    })
});

if(document.querySelector(".imageContent--showcolors")) {
    let icolors = document.querySelector(".imageContent--showcolors");
    
    let isideimage = icolors.querySelector(".imageContent__image img");
    
    let ibuttons = icolors.querySelectorAll(".imageContent__colors_tile");
    
    ibuttons.forEach(button=> {
        button.addEventListener("click", e=> {
            e.preventDefault();
            let newsrc = button.querySelector(".imageContent__colors--sideImage").src;
    
            isideimage.src = newsrc;
        })
    })

}