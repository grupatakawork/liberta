document.onload = () => {};

let header = document.querySelector(".header");

let navButton = document.querySelector(".header__bar--navButton");

let aside_items = document.querySelectorAll(".nav__aside_item");

let nav__items = document.querySelectorAll(".nav__item");


let adjustNavHeight = ()=>{
    
    
    if(header.classList.contains("unrolled")) {
        let headNav = document.querySelector(".header.unrolled .header__navigation");
        let submenu = document.querySelector(".nav__aside_item.active .nav__aside_item--submenu");
        let maxHeight = submenu.clientHeight + 100;
        if(maxHeight < window.innerHeight)
            maxHeight = window.innerHeight;
        headNav.querySelector("#navigation").style = "height:"+maxHeight+"px;";
    }
}


navButton.addEventListener("click", ()=>{
    header.classList.toggle("unrolled");
    if(header.classList.contains("unrolled")) {
        adjustNavHeight();
    }
});

aside_items.forEach(item => {
    item.querySelector("a").addEventListener("click", clicked => {
        
        nav__items.forEach(item=>{
            item.classList.remove("active")
        })
        nav__items[0].classList.add("active");

        if(item.querySelector(".nav__aside_item--submenu") ) {
            clicked.preventDefault();
            aside_items.forEach(it=>{it.classList.remove("active")});
            item.classList.add("active")
        }
        
        adjustNavHeight();
    })
})

let bottommenulinks = document.querySelectorAll("a.nav__tile--bottommenulink");

bottommenulinks.forEach(tile => {
    
    tile.addEventListener("click", click => {

        if(!tile.classList.contains("nav__tile--active")) {

            click.preventDefault();
            if (tile.parentNode.querySelector(".nav__tile--active"))
                tile.parentNode.querySelector(".nav__tile--active").classList.remove("nav__tile--active");
            tile.classList.add("nav__tile--active");
        }
        
        adjustNavHeight();
    })
})


nav__items.forEach(nav__item => {
    nav__item.addEventListener("click", click=> {
        nav__items.forEach(item=>{
            item.classList.remove("active")
        })
        nav__item.classList.add("active");

        if(nav__item.classList.contains("nav__item--onas")) {
            aside_items.forEach(it=>{it.classList.remove("active")});
            document.querySelector(".nav__aside_item--hidden").parentNode.classList.add("active");
        }
        if(nav__item.classList.contains("nav__item--produkty")) {
            aside_items.forEach(it=>{it.classList.remove("active")});
            aside_items[0].classList.add("active");
        }
        
        adjustNavHeight();
    })
})

window.onresize = adjustNavHeight;