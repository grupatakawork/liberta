import Swiper from 'swiper/bundle';

let homeslider__swiper = new Swiper('.homeslider__swiper', {
  spaceBetween: 30,
  effect: 'fade',
  centeredSlides: true,
  loop: true,
  speed: 2000,
  autoplay: {
    delay: 6000,
    disableOnInteraction: false,
  }
});

let choose__swiper = new Swiper('.choose__swiper', {
  slidesPerView: 2,
  spaceBetween: 30,
  navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
  },
  autoplay: {
    delay: 5000,
    disableOnInteraction: false,
  }
});

let partners_swiper = new Swiper('.partners__swiper', {
  slidesPerView: 'auto',
  spaceBetween: 10,
  loop: true,
  autoplay: {
    delay: 4000,
    disableOnInteraction: false,
  },
  pagination: {
    el: '.swiper-pagination',
  },
});

let opinions_swiper = new Swiper('.opinions__swiper', {
  slidesPerView: 'auto',
  autoHeight: true,
  spaceBetween: 50,
  loop: true,
  centeredSlides: true,
  autoplay: {
    delay: 5000,
    disableOnInteraction: false,
  },
  pagination: {
    el: '.swiper-pagination',
  },
});

let realizations__swiper = new Swiper('.realizations__swiper', {
  slidesPerView: 2,
  spaceBetween: 100,
  loop: true,
  centeredSlides: true,
  autoplay: {
    delay: 5000,
    disableOnInteraction: false,
  },
  navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
  },
});