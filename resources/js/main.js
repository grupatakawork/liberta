import $ from "jquery";
import "bootstrap/js/dist/util";
import "bootstrap/js/dist/carousel";
import "bootstrap/js/dist/tab";
import "bootstrap/js/dist/collapse";

import "./components/swiper";
import "./components/scripts";
import "./components/navigation";

import "lightbox2/dist/js/lightbox.min.js";